npm-fetch() {
	TARBALL=`curl https://registry.npmjs.org/$1 | jq -r '.versions[."dist-tags".latest].dist.tarball'`;
	wget --content-disposition $TARBALL;
}
